#!/bin/sh   
# Instalacao do Zabbix Agent para CentOS 8
# Criado em 10 de Agosto de 2020
# VARIAVEIS
NOMEMAQ= $(hostname)
dnf install https://repo.zabbix.com/zabbix/4.4/rhel/8/x86_64/zabbix-release-4.4-1.el8.noarch.rpm -y
dnf install zabbix zabbix-agent

cp /etc/zabbix/zabbix_agentd.conf ./zabbix_agentd.conf.bkp
sed -i 's/^#DebugLevel=3/DebugLevel=3/' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^#RefreshActiveChecks=120/RefreshActiveChecks=3600/' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^#EnableRemoteCommands=0/EnableRemoteCommands=1/' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^Server=127.0.0.1/Server=192.168.8.4/' /etc/zabbix/zabbix_agentd.conf
sed -i 's/^Hostname=Zabbix server/Hostname='$NOMEMAQ'/' /etc/zabbix/zabbix_agentd.conf
mkdir /var/run/zabbix
chown zabbix.zabbix /var/run/zabbix/
zabbix_agentd -c /etc/zabbix/zabbix_agentd.conf

firewall-cmd --permanent --add-port=10050/tcp
firewall-cmd --reload

systemctl start zabbix-agent
systemctl enable zabbix-agent

