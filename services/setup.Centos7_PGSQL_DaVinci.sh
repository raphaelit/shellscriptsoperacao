#!/bin/sh
#Instalacao do POSTGRES 12 no CENTOS 7
#Criado por Raphael Maria
#Em 17 de Março de 2020
#
#############################################################
# Criado para instalacao do SERVIDOR PARA DAVINCI STUDIO
#############################################################


####################################################################
##########           INSTALACAO BASICA E ESSENCIAL
####################################################################
yum -y install dialog wget tar unzip vim make gcc dnf autoconf automake epel-release 
yum -y groupinstall "Fonts"
export FONTCONFIG_PATH=/etc/fonts

##### VARIAVEIS ######
varinterface=$(nmcli con show | tail -1 | awk '{print $1}')

##### DIALOG ######
varhost=$(dialog --stdout --inputbox 'Insira o nome  do hostname desta maquina: ' 0 0)
varipaddress=$(dialog --stdout --inputbox 'Insira o IP ADDRESS do hostname desta maquina: ' 0 0)

#### setup hostname
hostnamectl set-hostname $varhost.o2pos.com.br 
yum update -y
yum upgrade -y 

#### Setup Network
nmcli con modify $varinterface ipv4.method manual ipv4.addresses $varipaddress/16 ipv4.gateway 192.168.8.1 ipv4.dns 192.168.8.15,192.168.8.16 ipv4.dns-search o2pos.com 
nmcli con up $varinterface


################################################################
###            Instalacao do DATABASE POSTGRES 12
################################################################
### Fonte: https://www.postgresql.org/download/linux/redhat/
#################################################################

yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum -y install postgresql12
yum -y install postgresql12-server
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable postgresql-12
systemctl start postgresql-12


yum -y install cockpit
systemctl enable --now cockpit.socket

#####################################################################
###           COMO MOVER O BANCO DE DADOS DE LUGAR
#####################################################################
### Fonte: https://help.cloud66.com/rails/how-to-guides/databases/shells/change-psql-directory.html
#####################################################################

systemctl stop postgresql-12
mkdir -p /database/resolve
chown postgres /database/resolve
chmod 700 /database/resolve
mv /var/lib/pgsql/12/data /database/resolve/
ln -s /database/resolve/data /var/lib/pgsql/12/data
systemctl start postgresql-12

firewall-cmd --permanent --zone=trusted --add-source=10.1.1.0/24
firewall-cmd --permanent --zone=trusted --add-source=192.168.0.0/16
firewall-cmd --permanent --zone=trusted --add-port=5432/tcp
firewall-cmd --permanent --zone=trusted --add-port=80/tcp
firewall-cmd --permanent --zone=trusted --add-port=8080/tcp
firewall-cmd --permanent --zone=public --add-service=cockpit
firewall-cmd --permanent --add-service=http
firewall-cmd --reload

systemctl restart postgresql-12
systemctl start httpd
systemctl enable httpd

####################################################################
######                 Install PGADMIN 4                       #####
####################################################################
##### FONTE https://www.tecmint.com/install-pgadmin4-in-centos-7/ ##
####################################################################
yum -y install pgadmin4

cp /etc/httpd/conf.d/pgadmin4.conf.sample /etc/httpd/conf.d/pgadmin4.conf

'''
<VirtualHost *:80>
LoadModule wsgi_module modules/mod_wsgi.so
WSGIDaemonProcess pgadmin processes=1 threads=25
WSGIScriptAlias /pgadmin4 /usr/lib/python2.7/site-packages/pgadmin4-web/pgAdmin4.wsgi

<Directory /usr/lib/python2.7/site-packages/pgadmin4-web/>
        WSGIProcessGroup pgadmin
        WSGIApplicationGroup %{GLOBAL}
        <IfModule mod_authz_core.c>
                # Apache 2.4
                Require all granted
        </IfModule>
        <IfModule !mod_authz_core.c>
                # Apache 2.2
                Order Deny,Allow
                Deny from All
                Allow from 127.0.0.1
                Allow from ::1
        </IfModule>
</Directory>
</VirtualHost>
'''
mkdir -p /var/lib/pgadmin4/
mkdir -p /var/log/pgadmin4/
chown -R apache:apache /var/lib/pgadmin4/*
chown -R apache:apache /var/log/pgadmin4/*

# vi /usr/lib/python3.6/site-packages/pgadmin4-web/config_local.py
# vi /usr/lib/python2.7/site-packages/pgadmin4-web/config_distro.py
'''
LOG_FILE = '/var/log/pgadmin4/pgadmin4.log'
SQLITE_PATH = '/var/lib/pgadmin4/pgadmin4.db'
SESSION_DB_PATH = '/var/lib/pgadmin4/sessions'
STORAGE_DIR = '/var/lib/pgadmin4/storage'
'''
# python /usr/lib/python2.7/site-packages/pgadmin4-web/setup.py
# python /usr/lib/python3.6/site-packages/pgadmin4-web/setup.py

dialog --msgbox "Sua configuração iniciar foi concluida \nAcesse este dispositivo através do endereço \nhttps://$varipaddress:9090  \nusando o usuário de logado desta sessão do shell" 0 0