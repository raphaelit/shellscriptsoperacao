 #!/bin/sh
### INSTALACAO DO SERVIDOR DE GLPI
### VERSAO PARA CENTOS 8
# CRIADO POR RAPHAEL MARIA.
# EM 05 DE OUTUBRO DE 2020.
# VERSAO 1.0
'''
FONTE #1: https://medium.com/@isaqueprofeta/passos-de-instala%C3%A7%C3%A3o-do-glpi-9-5-3ac1e8a27f1f
FONTE #2: https://www.youtube.com/watch?v=6ZiH7IKQ-xs&ab_channel=HappyGhost
FONTE #3: https://www.youtube.com/watch?v=ll-EgIZgXkk&feature=emb_title&ab_channel=IsaqueProfeta
'''
##### VARIAVEIS
VARHOSTNAME=$(dialog --stdout --inputbox 'Insira o nome  do hostname desta maquina: ' 0 0)
VARIPADDRESS=$(dialog --stdout --inputbox 'Insira o IP ADDRESS do hostname desta maquina: ' 0 0)
VARGATEWAY=$(dialog --stdout --inputbox 'Insira o GATEWAY do hostname desta rede: ' 0 0)

# Altera somente o IP Address de DHCP para FIXO com o ip designado anterimente.
VARINTERFACE=$(nmcli con show | tail -1 | awk '{print $1}')
nmcli con modify $VARINTERFACE ipv4.method manual ipv4.addresses $VARIPADDRESS/16 ipv4.gateway 192.168.8.1 ipv4.dns 192.168.8.100,192.168.8.110,192.168.8.15 ipv4.dns-search o2pos.com.br
nmcli con up $VARINTERFACE

hostnamectl set-hostname $VARHOSTNAME.o2pos.com.br
timedatectl set-timezone America/Sao_Paulo

dnf module install mariadb
systemctl start mariadb && systemctl enable mariadb

mysql_secure_installation << EOF

Y
Temporary2020!
Temporary2020!
Y
Y
Y
Y
EOF

mysql -u root -p$DBPASS -e "CREATE USER 'glpi'@'%' IDENTIFIED BY 'MinhaSenhaDoGLPI';"
mysql -u root -p$DBPASS -e "GRANT USAGE ON *.* TO 'glpi'@'%' IDENTIFIED BY 'MinhaSenhaDoGLPI';"
mysql -u root -p$DBPASS -e "CREATE DATABASE IF NOT EXISTS `glpi` ;"
mysql -u root -p$DBPASS -e "GRANT ALL PRIVILEGES ON `glpi`.* TO 'glpi'@'%';"
mysql -u root -p$DBPASS -e "FLUSH PRIVILEGES;"

dnf install epel-release
dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
dnf module reset php
dnf module enable php:remi-7.4

dnf install wget tar zip bzip2 httpd php php-{curl,fileinfo,gd,json,mbstring,mysqli,session,zlib,simplexml,xml,cli,domxml,imap,ldap,openssl,xmlrpc,pecl-apcu,pecl-zip,intl}
cd /tmp
wget https://github.com/glpi-project/glpi/releases/download/9.5.1/glpi-9.5.1.tgz
tar xzf glpi-9.5.1.tgz -C /var/www/html/
chown -R apache:apache /var/www/html/glpi
chmod -R 755 /var/www/html/glpi/files/
chmod -R 755 /var/www/html/glpi/config/

echo "<VirtualHost *:80>
   ServerName glpi.local
   DocumentRoot /var/www/html/glpi
   ErrorLog "/var/log/httpd/glpi_error.log"
   CustomLog "/var/log/httpd/glpi_access.log" combined
<Directory> /var/www/html/glpi/config>
           AllowOverride None
           Require all denied
   </Directory>
   <Directory> /var/www/html/glpi/files>
           AllowOverride None
           Require all denied
   </Directory>
</VirtualHost>" >> /etc/httpd/conf.d/glpi.conf

systemctl start httpd && systemctl enable httpd
setsebool -P httpd_unified 1
setsebool -P httpd_can_network_connect 1
setsebool -P httpd_graceful_shutdown 1
setsebool -P httpd_can_network_relay 1
setsebool -P nis_enabled 1
setsebool -P httpd_can_network_connect_db 1
setsebool -P httpd_can_sendmail on
firewall-cmd --add-service={http,https} --permanent
firewall-cmd --reload

dialog --msgbox --stdout "Abra seu navegador, acesse o IP:$VARINTERFACE/glpi e efetuei a configuração, ao terminar pressione OK" 0 0 ;
mv /var/www/html/glpi/install/install.php /var/www/html/glpi/install/install.php.bak
